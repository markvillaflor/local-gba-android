package mcig.base.domain

import com.google.gson.annotations.SerializedName

/**
 * Created by republisys on 9/25/17.
 */

abstract class BaseRaw {

    @SerializedName("id")
    abstract fun id(): String?

}

