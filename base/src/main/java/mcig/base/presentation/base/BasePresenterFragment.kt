package mcig.base.presentation.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.util.Log
import dagger.android.support.AndroidSupportInjection

/**
 * Created by gerald.tayag on 2/9/2017.
 */
abstract class BasePresenterFragment<P : BasePresenter<V>, V : BaseView> : BaseFragment(), LoaderManager.LoaderCallbacks<P> {

    protected var presenter: P? = null

    private var savedInstanceState: Bundle? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(injectComponents())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.savedInstanceState = savedInstanceState

        if (this.loaderManager.getLoader<P>(LOADER_ID) == null){
            loaderManager.initLoader(LOADER_ID, Bundle(), this)
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle): Loader<P> {
        return BasePresenterLoader.newInstance(context, createPresenterFactory())
    }

    override fun onLoadFinished(loader: Loader<P>, basePresenter: P) {
        val presenterView = presenterView
        presenter = basePresenter

        if (null != presenterView) {
            presenter!!.attachView(presenterView)
        } else {
            Log.d(TAG, "View can't be attached because you don't implement it in your fragment.")
        }

        Log.e(TAG, "Fragment onPresenterCreated")
        onPresenterCreated(savedInstanceState)
    }

    override fun onLoaderReset(loader: Loader<P>) {
        onPresenterDestroyed()
        if (isPresenterAvailable) {
            presenter!!.detachView()
            presenter = null
        }
    }

    protected abstract fun createPresenterFactory(): BasePresenterFactory<P>

    protected abstract fun onPresenterCreated(savedInstanceState: Bundle?)

    protected abstract fun onPresenterDestroyed()

    protected val isPresenterAvailable: Boolean
        get() = presenter != null

    protected val presenterView: V?
        get() {
            var view: V? = null

            try {
                view = this as V
            } catch (ex: ClassCastException) {
                Log.e(TAG, "You should implement your view class in the fragment.", ex.cause)
            }

            return view
        }

    companion object {

        private val TAG = BasePresenterFragment::class.java.simpleName
        private val LOADER_ID = 201
    }

    abstract fun injectComponents(): Fragment
}
