package mcig.base.presentation.base

import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import mcig.base.presentation.viewmodel.BaseViewModel

import android.os.Bundle
import android.os.SystemClock
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by gerald.tayag on 2/9/2017.
 */

abstract class BasePresenterActivity<BINDING : ViewDataBinding, VM : BaseViewModel, P : BasePresenter<V>, V : BaseView> : BaseActivity(),
        LoaderManager.LoaderCallbacks<P> {

    companion object {
        val TAG = BasePresenterActivity::class.java.simpleName
        private val LOADER_ID = 201
    }

    var presenter: P? = null
        private set

    lateinit var viewModel: VM
    var binding: BINDING? = null

    private var mLastClickTime: Long = 0

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        viewModel = createViewModel()
        setViewModel(binding!!, viewModel)

        if (supportLoaderManager.getLoader<P>(LOADER_ID) == null) {
            supportLoaderManager.initLoader(LOADER_ID, Bundle(), this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (binding != null) binding!!.unbind()
    }

    override fun onCreateLoader(id: Int, args: Bundle): Loader<P> {
        return BasePresenterLoader.newInstance(applicationContext, createPresenterFactory())
    }

    override fun onLoadFinished(loader: Loader<P>, basePresenter: P) {
        presenter = basePresenter

        if (null != presenterView) {
            presenter!!.attachView(presenterView!!)
        } else {
            Log.d(TAG, "View can't be attached because you don't implement it in your activity.")
        }

        onPresenterCreated()
        Log.e("Presenter", this.javaClass.simpleName + " Created")
    }

    override fun onLoaderReset(loader: Loader<P>) {
        onPresenterDestroyed()

        if (isPresenterAvailable) {
            presenter!!.detachView()
            presenter = null
        }
    }


    protected abstract fun createViewModel(): VM
    protected abstract fun setViewModel(binding: BINDING, viewModel: VM)
    protected abstract fun createPresenterFactory(): BasePresenterFactory<P>
    protected abstract fun onPresenterCreated()
    protected abstract fun onPresenterDestroyed()

    protected val isPresenterAvailable: Boolean
        get() = presenter != null

    protected val presenterView: V?
        get() {
            var view: V? = null

            try {
                view = this as V
            } catch (ex: ClassCastException) {
                Log.e(TAG, "You should implement your view class in the activity.", ex.cause)
            }

            return view
        }

    fun delayClick(): Boolean {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return false
        }

        mLastClickTime = SystemClock.elapsedRealtime()
        return true
    }

    inline fun View.onClick(crossinline function: () -> Unit) {
        this.setOnClickListener {
            function.invoke()
        }
    }

    fun initGlobalLayoutListener(mRootLayout: View, onGlobalLayout: ViewTreeObserver.OnGlobalLayoutListener) {
        var viewTreeObserver: ViewTreeObserver = mRootLayout.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            return viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayout)
        }
    }

}
