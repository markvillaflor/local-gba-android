package mcig.base.presentation.base

import io.reactivex.disposables.Disposable

/**
 * Created by gerald.tayag on 2/9/2017.
 */

abstract class BasePresenter<V : BaseView> {

    var view: V? = null
        private set

    var disposable: Disposable? = null

    fun attachView(view: V) {
        this.view = view

        onPresenterCreated()
    }

    fun detachView() {
        this.view = null
        dispose()
    }

    private fun dispose() {
        if (disposable != null && !disposable!!.isDisposed) {
            disposable!!.dispose()
        }
    }

    val isViewAttached: Boolean
        get() = this.view != null

    protected abstract fun onPresenterCreated()
}