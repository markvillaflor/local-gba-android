package mcig.base.data.store

import io.reactivex.Flowable
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmObject
import mcig.base.domain.BaseEntity
import polanski.option.Option


/**
 * Created by republisys on 9/26/17.
 */
class RealmDataStore<T : RealmObject, R: BaseEntity>(mRealm: Realm) {

    private var table: Class<T>? = null
        get
    private var model: R? = null
    private var mRealm: Realm? = mRealm

    fun toRealmSave(mObject: T) {
        mRealm!!.beginTransaction()
        mRealm!!.copyToRealmOrUpdate(mObject)
        mRealm!!.commitTransaction()
    }

    fun toRealmResultSave(function: () -> Unit) {
        function.invoke()
    }

    fun all(): Flowable<Option<List<T>>> {
        return Flowable.just(Option.ofObj(mRealm!!.where(table).findAll().toList()))
    }

    fun realmClose(){
        mRealm!!.close()
    }
}