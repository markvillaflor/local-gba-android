package mcig.base.util.validator.rules

/**
 * Created by republisys on 10/12/17.
 */
class MinLengthValidator(val min: Int, error: String) : Validator<String>(error) {

    override fun isValid(t: String?): Boolean = t != null && t.length >= min

}