package mcig.base.util.validator.rules

import android.util.Log
import mcig.base.util.validator.ObservableValidatorField

/**
 * Created by republisys on 10/12/17.
 */

open class SameAsValidator constructor(val value: ObservableValidatorField<String>, error: String) : Validator<String>(error) {

    override fun isValid(t: String?): Boolean {
        Log.e("SameAsValidator", "$t  - $value")

        val isValid = t != null && t.compareTo(value.value!!) == 0

        if (isValid) value.hideErrorMessage()

        return isValid
    }

}
