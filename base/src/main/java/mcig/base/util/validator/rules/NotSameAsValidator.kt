package mcig.base.util.validator.rules

import mcig.base.util.validator.BaseValidator
import mcig.base.util.validator.ObservableValidatorField

/**
 * Created by republisys on 10/13/17.
 */
class NotSameAsValidator(val value: ObservableValidatorField<String>, val error: String) : BaseValidator<String> {

    override fun isValid(t: String?): Boolean {
        return t != null && t != value.value
    }

    override fun getErrorMessage(): String = error
}