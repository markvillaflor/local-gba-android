package mcig.base.util.validator

/**
 * Created by republisys on 10/12/17.
 */
interface BaseValidator<T> {

    fun isValid(t: T?): Boolean
    fun getErrorMessage(): String

}