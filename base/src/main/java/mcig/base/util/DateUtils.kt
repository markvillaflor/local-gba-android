package mcig.base.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by republisys on 10/9/17.
 */
open class DateUtils {

    val timeInstance = Calendar.getInstance()

    companion object {

        val `DD-MM-YYYY` = "dd-MM-yyyy"
    }

    fun getCurrentDate(): Date {
        return Date(timeInstance.timeInMillis)
    }

    fun convertStringToDate(from: Format, dateString: String): Date? {
        return try {
            val currentDateFormat = SimpleDateFormat(from.format, Locale.getDefault())
            currentDateFormat.parse(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
            null
        }
    }

    fun convertDateToString(from: Format, to: Format, dateString: String): String {
        return try {
            val currentDateFormat = SimpleDateFormat(from.format, Locale.getDefault())
            val newDateString = currentDateFormat.parse(dateString)

            val newDateFormat = SimpleDateFormat(to.format, Locale.getDefault())
            newDateFormat.format(newDateString)
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }

    fun convertDateToString(from: Format, dateString: Date): String {
        return try {
            val currentDateFormat = SimpleDateFormat(from.format, Locale.getDefault())
           currentDateFormat.format(dateString)
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }
}

enum class Format(val format: String) {

    `yyyy-MM-dd`("yyyy-MM-dd"),
    `dd-MM-yyyy`("dd-MM-yyyy"),
    `MM-dd-yyyy`("MM-dd-yyyy")

}