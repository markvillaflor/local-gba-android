package estansaas.fonebayad.utils

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.util.Log
import com.afollestad.materialdialogs.GravityEnum
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.Theme
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by gerald.tayag on 3/28/2017.
 */

open class DialogBuilder(var mContext: Context) {

    private var mMaterialDialog: MaterialDialog? = null

    companion object {
        infix fun initialize(mContext: Context): DialogBuilder {
            return DialogBuilder(mContext)
        }
    }

    /**
     * Creating Neutral Dialog with resources title, content
     */
    fun createDialog(title: Int, content: Int, neutral: Int) {
        var mTitle: String? = ""
        if (title != 0) {
            mTitle = mContext.resources.getString(title)
        }
        createDialog(mTitle, mContext.resources.getString(content), mContext.resources.getString(neutral), "", "", null)
    }

    /**
     * Creating Neutral Dialog with String title, content
     */
    fun createDialog(title: String, content: String, neutral: String) {
        createDialog(title, content, neutral, "", "", null)
    }

    /**
     * Creating Neutral Dialog with String title, content with SingleButtonCallback
     */
    fun createDialog(title: String, content: String, positive: String, callback: MaterialDialog.SingleButtonCallback?) {
        createDialog(title, content, "", "", positive, callback)
    }

    /**
     * Creating Neutral Dialog with resources title, content with SingleButtonCallback
     */
    fun createDialog(title: Int, content: Int, neutral: Int, callback: MaterialDialog.SingleButtonCallback? = null) {
        var mTitle: String? = ""
        if (title != 0) {
            mTitle = mContext.resources.getString(title)
        }
        createDialog(mTitle, mContext.resources.getString(content), mContext.resources.getString(neutral), "", "", callback)
    }

    /**
     * Creating Neutral Dialog with resources title, content with SingleButtonCallback
     */
    fun createDialog(title: Int, content: Int, negative: Int, positive: Int, callback: MaterialDialog.SingleButtonCallback? = null) {
        var mTitle: String? = ""
        if (title != 0) {
            mTitle = mContext.resources.getString(title)
        }
        createDialog(mTitle, mContext.resources.getString(content), "", mContext.resources.getString(positive), mContext.resources.getString(negative), callback)
    }

    fun createDialog(title: String, content: String, negative: String, positive: String, callback: MaterialDialog.SingleButtonCallback? = null) {
        createDialog(title, content, "", positive, negative, callback)
    }

    fun createDialog(title: Int, content: Int, neutral: Int, positive: Int, negative: Int, callback: MaterialDialog.SingleButtonCallback? = null) {
        var mTitle: String? = ""
        if (title != 0) {
            mTitle = mContext.resources.getString(title)
        }
        createDialog(mTitle, mContext.resources.getString(content), mContext.resources.getString(neutral), mContext.resources.getString(positive), mContext.resources.getString(negative), callback)
    }

    @JvmOverloads fun createDialog(title: String?, content: String?, neutral: String?, positive: String?, negative: String?, callback: MaterialDialog.SingleButtonCallback? = null) {
        val materialBuilder = MaterialDialog.Builder(mContext)

        materialBuilder.content(content!!)
        materialBuilder.cancelable(false)
        materialBuilder.theme(Theme.LIGHT)

        materialBuilder.contentColor(Color.BLACK)
        materialBuilder.canceledOnTouchOutside(false)

        Log.e("onCreateDialog", TextUtils.isEmpty(neutral).toString())

        if (!TextUtils.isEmpty(title)) materialBuilder.title(title!!)
        if (!TextUtils.isEmpty(neutral)) materialBuilder.neutralText(neutral!!)
        if (!TextUtils.isEmpty(negative)) materialBuilder.negativeText(negative!!)
        if (!TextUtils.isEmpty(positive)) materialBuilder.positiveText(positive!!)

        if (callback != null) materialBuilder.onAny(callback)

        materialBuilder.buttonsGravity(GravityEnum.END)

//        materialBuilder.neutralColor(ContextCompat.getColor(mContext, R.color.shark))
//        materialBuilder.positiveColor(ContextCompat.getColor(mContext, R.color.orient))
//        materialBuilder.negativeColor(ContextCompat.getColor(mContext, R.color.shark))

        mMaterialDialog = materialBuilder.build()
        mMaterialDialog!!.show()
    }

    fun createLoadingDialog(title: Int, content: Int, function: () -> Unit) {

        val materialBuilder = MaterialDialog.Builder(mContext)

        if (title != 0) materialBuilder.title(title)

        materialBuilder.content(content)
        materialBuilder.contentGravity(GravityEnum.CENTER)
        materialBuilder.theme(Theme.LIGHT)
//        materialBuilder.widgetColor(ContextCompat.getColor(mContext, R.color.orient))
        materialBuilder.progressIndeterminateStyle(false)
        materialBuilder.progress(true, 0)
        materialBuilder.cancelable(false)
        materialBuilder.canceledOnTouchOutside(false)

        mMaterialDialog = materialBuilder.build()
        mMaterialDialog!!.setOnShowListener({
            function.invoke()
        })
        mMaterialDialog!!.show()
    }

   /* fun showDatePickerDialog(editText: AppCompatEditText, min_date: Long, max_date: Long, calendar: Calendar) {

        val dateFormatter = SimpleDateFormat("MM/dd/yyyy")

        val fromDatePickerDialog = DatePickerDialog(mContext, { _, year1, monthOfYear, dayOfMonth ->

            val newDate = Calendar.getInstance()
            newDate.set(year1, monthOfYear, dayOfMonth)

            Log.e("showDatePicker", dateFormatter.format(newDate.time) + " sadsds")
            editText.setText(dateFormatter.format(newDate.time))

        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))

        fromDatePickerDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        fromDatePickerDialog.datePicker.minDate = min_date
        fromDatePickerDialog.datePicker.maxDate = max_date

        fromDatePickerDialog.show()
    }*/
   fun showDatePicker(format: String, min_date: Long, max_date: Long, calendar: Calendar, onDateSetListener: DatePickerDialog.OnDateSetListener) {

       val dateFormatter = SimpleDateFormat(format)

       val fromDatePickerDialog = DatePickerDialog(mContext, onDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
       fromDatePickerDialog.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
       fromDatePickerDialog.datePicker.minDate = min_date
       fromDatePickerDialog.datePicker.maxDate = max_date

       fromDatePickerDialog.show()
   }


//    fun createImagePickerDialog(imagePickResult: ImagePickResult): ImageDialog {
//        val imageSetup = ImageSetup()
//        imageSetup.title = mContext!!.resources.getString(R.string.title_upload_photo)
//        imageSetup.galleryIcon = R.drawable.gallery
//        imageSetup.cameraIcon = R.drawable.camera
//        imageSetup.buttonOrientation = LinearLayout.HORIZONTAL
//        imageSetup.setPickTypes(ImageType.CAMERA, ImageType.GALLERY)
//        imageSetup.setFlip(false)
//        imageSetup.dimAmount = 0.5f
//        return ImageDialog.build(imageSetup, imagePickResult)
//    }

    fun dismiss() {

        if (mMaterialDialog == null) return

        if (mMaterialDialog!!.isShowing) {
            mMaterialDialog!!.dismiss()
        }
    }

    private val isShowing: Boolean
        get() {
            if (mMaterialDialog!!.isShowing) {
                return true
            }
            return false
        }
}
