package mcig.base.common.providers

/**
 * Class to be able to test timestamp related features. Inject this instead of using System.currentTimeMillis()
 */
class TimestampProvider {

    fun currentTimeMillis(): Long {
        return System.currentTimeMillis()
    }
}
