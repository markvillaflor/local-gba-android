package republisys.globalbankaccess.module.presentation.login

import mcig.module.interactor.UserEntityInteractor
import mcig.module.presentation.login.LoginPresenter
import mcig.module.repository.UserRepository
import org.junit.Assert.*
import org.junit.Before
import republisys.globalbankaccess.module.test_commom.BaseTest
import republisys.globalbankaccess.module.test_commom.InteractorBaseTest

/**
 * Created by republisys on 10/24/17.
 */
class LoginPresenterTest : InteractorBaseTest() {

    lateinit var userRepository: UserRepository
    lateinit var userInteractor: UserEntityInteractor
    lateinit var loginPresenter: LoginPresenter

    override fun onSetUp() {
        userRepository = UserRepository(restService = restService!!)
        userInteractor = UserEntityInteractor(userRepository)
        loginPresenter = LoginPresenter(userInteractor)
    }


}