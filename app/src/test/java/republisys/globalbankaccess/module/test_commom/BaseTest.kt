package republisys.globalbankaccess.module.test_commom

import io.reactivex.subscribers.TestSubscriber
import mcig.module.domain.RestService
import mcig.module.test_commom.RxSchedulerOverrideRule
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by republisys on 10/24/17.
 */
@RunWith(MockitoJUnitRunner.StrictStubs::class)
abstract class BaseTest<T> {

    @get:Rule
    var rule = MockitoJUnit.rule()

    @get:Rule
    var overrideSchedulersRule = RxSchedulerOverrideRule()

    val testSubscriber = TestSubscriber.create<T>()

}