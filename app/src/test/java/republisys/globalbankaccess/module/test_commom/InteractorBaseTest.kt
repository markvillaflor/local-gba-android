package republisys.globalbankaccess.module.test_commom

import com.google.gson.GsonBuilder
import io.reactivex.subscribers.TestSubscriber
import mcig.base.BuildConfig
import mcig.module.data.typeAdapter.EntityTypeAdapterFactory
import mcig.module.domain.RestService
import mcig.module.test_commom.RxSchedulerOverrideRule
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

// FIXME: This class should be in base module but the project can't compile when is placed there.
@RunWith(MockitoJUnitRunner.StrictStubs::class)
abstract class InteractorBaseTest {

    @get:Rule
    var rule = MockitoJUnit.rule()

    @get:Rule
    var overrideSchedulersRule = RxSchedulerOverrideRule()

    var restService: RestService? = null

    @Before
    fun setUp() {

        val okBuilder = OkHttpClient.Builder()

        okBuilder.connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)

        okBuilder.interceptors().add(Interceptor {

            val original = it.request()
            val request = original.newBuilder()
                    .header("Accept", "application/json")
                    .header("Cache-Control", "private, must-revalidate")
                    .method(original.method(), original.body())

            return@Interceptor it.proceed(request.build())
        })

        val builder = GsonBuilder()
        builder.registerTypeAdapterFactory(EntityTypeAdapterFactory.create())

        val retrofit = Retrofit.Builder()
                .callFactory(okBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(builder.create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.gba_STAGING_API_URL).build()

        restService = retrofit.create(RestService::class.java)

        onSetUp()
    }

    protected abstract fun onSetUp()
}
