package republisys.globalbankaccess.injection.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import mcig.base.BuildConfig
import mcig.base.common.rx.RxErrorHandlingCallAdapterFactory
import mcig.module.data.typeAdapter.EntityTypeAdapterFactory
import mcig.module.domain.RestService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by republisys on 10/23/17.
 */

@Module
class NetworkModule {

    @Provides
    fun provideTypeAdapterFactory(): EntityTypeAdapterFactory {
        return EntityTypeAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideGson(typeAdapters: EntityTypeAdapterFactory): Gson {
        val builder = GsonBuilder()
        builder.registerTypeAdapterFactory(typeAdapters)
        return builder.create()
    }

    @Provides
    @Singleton
    fun provideApiOkHttpClient(): OkHttpClient {
        val okBuilder = OkHttpClient.Builder()

        okBuilder.connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)

        okBuilder.interceptors().add(Interceptor {

            val original = it.request()
            val request = original.newBuilder()
                    .header("Accept", "application/json")
                    .header("Cache-Control", "private, must-revalidate")
                    .method(original.method(), original.body())

            return@Interceptor it.proceed(request.build())
        })

        return okBuilder.build()
    }

    @Provides
    @Singleton
    fun providedRestService(okHttpClient: OkHttpClient, gson: Gson): RestService {

        val httpClientBuilder = okHttpClient.newBuilder()

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(loggingInterceptor)
        }

        return Retrofit.Builder()
                .baseUrl(BuildConfig.gba_STAGING_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .callFactory(httpClientBuilder.build())
                .build().create(RestService::class.java)
    }
}