package mcig.a420cloud.application.modules

import dagger.Module
import dagger.Provides

import mcig.module.domain.RestService
import mcig.module.repository.UserRepository
import republisys.globalbankaccess.injection.module.NetworkModule

/**
 * Created by republisys on 7/18/17.
 */

@Module(includes = arrayOf(NetworkModule::class))
class RepositoryBuilderModule {

    @Provides
    fun providedUserRepository(restApi: RestService): UserRepository = UserRepository(restApi)

}
