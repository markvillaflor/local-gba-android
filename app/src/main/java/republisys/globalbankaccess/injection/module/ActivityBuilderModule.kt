package republisys.globalbankaccess.injection.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import mcig.a420cloud.application.modules.PresenterFactoryModule
import mcig.module.presentation.login.LoginActivity

/**
 * Created by republisys on 10/23/17.
 */
@Module(includes = arrayOf(PresenterFactoryModule::class))
internal abstract class ActivityBuilderModule {

    @ContributesAndroidInjector
    internal abstract fun loginActivity(): LoginActivity

}