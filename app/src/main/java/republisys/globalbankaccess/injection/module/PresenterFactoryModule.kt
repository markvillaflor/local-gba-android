package mcig.a420cloud.application.modules

import dagger.Module
import dagger.Provides
import mcig.module.interactor.UserEntityInteractor
import mcig.module.presentation.login.LoginPresenterFactory

/**
 * Created by republisys on 7/19/17.
 */

@Module(includes = arrayOf(RepositoryBuilderModule::class))
class PresenterFactoryModule{

    @Provides
    fun entryPresenterFactory(userEntityInteractor: UserEntityInteractor): LoginPresenterFactory = LoginPresenterFactory(userEntityInteractor)

}
