package mcig.a420cloud.application.modules

import dagger.Module
import dagger.Provides
import mcig.module.interactor.UserEntityInteractor
import mcig.module.repository.UserRepository

/**
 * Created by republisys on 9/27/17.
 */

@Module(includes = arrayOf(RepositoryBuilderModule::class))
class InteractorBuilderModule {

    @Provides
    fun userInteractorProvided(userRepository: UserRepository): UserEntityInteractor = UserEntityInteractor(userRepository)

}