package republisys.globalbankaccess

import android.app.Activity
import android.app.Application
import android.content.Context
import android.util.Log

import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.realm.Realm
import republisys.globalbankaccess.injection.component.DaggerApplicationComponent
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by republisys on 10/23/17.
 */
class GlobalBankAccess : Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)

        initTimber()
        initializeComponent()
        initStetho()
        initLeakCanary()
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingInjector

    private fun initializeComponent() {
        DaggerApplicationComponent.builder().application(this).build().inject(this)
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                    Stetho.newInitializerBuilder(this)
                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                            .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build()).build())
        }
    }

    private fun initLeakCanary(): RefWatcher {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return RefWatcher.DISABLED
        }
        return LeakCanary.install(this)
    }

    private fun initTimber(){
        if(BuildConfig.DEBUG){
            Timber.plant(object : Timber.DebugTree() {
                override fun createStackElementTag(element: StackTraceElement): String {
                    return super.createStackElementTag(element) + " Line: " + element.lineNumber
                }
            })
        }
    }
}