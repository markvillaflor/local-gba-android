package mcig.module.presentation.login

import mcig.base.presentation.base.BaseView

/**
 * Created by republisys on 9/25/17.
 */
interface LoginView: BaseView {

    fun showMainActivity()

}