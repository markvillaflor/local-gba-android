package mcig.module.presentation.login

import android.app.Activity
import mcig.base.presentation.base.BasePresenterActivity
import mcig.base.presentation.base.BasePresenterFactory

import republisys.globalbankaccess.R
import republisys.globalbankaccess.databinding.ActivityLoginBinding
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */
class LoginActivity : BasePresenterActivity<ActivityLoginBinding, LoginViewModel, LoginPresenter, LoginView>(), LoginView {

    @Inject internal lateinit var loginPresenterFactory: LoginPresenterFactory

    override fun getLayoutId(): Int = R.layout.activity_login
    override fun injectComponents(): Activity = this

    override fun createPresenterFactory(): BasePresenterFactory<LoginPresenter> = loginPresenterFactory
    override fun createViewModel(): LoginViewModel = LoginViewModel()

    override fun setViewModel(binding: ActivityLoginBinding, viewModel: LoginViewModel) {
        binding.viewModel = viewModel
    }

    override fun onPresenterCreated() {
        Timber.tag("lifeCycles")
        Timber.i("onPresenterCreated")
        presenter!!.fetchAll()
    }

    override fun onPresenterDestroyed() {
        Timber.i("onPresenterDestroyed")
    }

    override fun showMainActivity() {
        Timber.i("showMainActivity")
    }
}