package mcig.module.presentation.login

import mcig.base.presentation.base.BasePresenterFactory
import mcig.module.interactor.UserEntityInteractor
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */
class LoginPresenterFactory @Inject constructor(private val userEntityInteractor: UserEntityInteractor) : BasePresenterFactory<LoginPresenter> {

    override fun create(): LoginPresenter {
        return LoginPresenter(userEntityInteractor)
    }

}