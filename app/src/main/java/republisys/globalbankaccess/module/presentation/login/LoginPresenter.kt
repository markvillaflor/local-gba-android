package mcig.module.presentation.login

import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mcig.base.presentation.base.BasePresenter
import mcig.module.interactor.UserEntityInteractor
import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */
class LoginPresenter @Inject constructor(private val userEntityInteractor: UserEntityInteractor) : BasePresenter<LoginView>() {

    override fun onPresenterCreated() {

    }

    fun fetchAll() {
        disposable = userEntityInteractor.getBehaviorStream(Option.none())
                .observeOn(Schedulers.computation())
                .subscribe({ this.onSuccess(it) }, { this.onError(it.message!!) })
    }

    private fun onSuccess(list: List<CountryEntity>) {
        view!!.showMainActivity()
    }

    private fun onError(string: String) {
        view!!.onError(string)
    }

}