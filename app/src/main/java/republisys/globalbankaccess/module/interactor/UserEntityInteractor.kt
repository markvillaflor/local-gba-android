package mcig.module.interactor

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import mcig.base.common.rx.UnwrapOptionTransformer
import mcig.base.domain.ReactiveInteractor
import mcig.module.repository.UserRepository

import polanski.option.Option
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import javax.inject.Inject

/**
 * Created by republisys on 9/25/17.
 */

/**
* Interactor is only concerned with those data access and processing calls that are required by the feature that it is serving.
*
* Method created in this class are Create, Read or Fetch, Update and Delete.
*
*/
class UserEntityInteractor @Inject constructor(val repository: UserRepository) :
        ReactiveInteractor.RetrieveInteractor<Void, List<CountryEntity>>,
        ReactiveInteractor.RequestInteractor<Void, List<CountryEntity>>,
        ReactiveInteractor.RefreshInteractor<Void> {

    override fun getBehaviorStream(params: Option<Void>): Flowable<List<CountryEntity>> {
        return repository.getAllFromCache().flatMapSingle { this.fetchWhenNoneAndThenDrafts(it) }.compose(UnwrapOptionTransformer.create())
    }

    override fun getSingle(params: Option<Void>): Single<List<CountryEntity>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getRefreshSingle(params: Option<Void>): Completable {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun fetchWhenNoneAndThenDrafts(drafts: Option<List<CountryEntity>>): Single<Option<List<CountryEntity>>> {
        return fetchWhenNone(drafts).andThen(Single.just(drafts))
    }

    private fun fetchWhenNone(drafts: Option<List<CountryEntity>>): Completable {
        return if (drafts.isNone)
            repository.fetchData()
        else
            Completable.complete()
    }
}