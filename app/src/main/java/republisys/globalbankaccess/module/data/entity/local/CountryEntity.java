package republisys.globalbankaccess.module.data.entity.local;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import mcig.base.domain.BaseEntity;

/**
 * Created by republisys on 9/27/17.
 */

@AutoValue
public abstract class CountryEntity extends BaseEntity  {

    @SerializedName("name")
    public abstract String name();

    @SerializedName("flag")
    public abstract String flag();

    @SerializedName("flag_rectangle")
    public abstract String flag_rectangle();

    @SerializedName("currency")
    public abstract String currency();

    @SerializedName("customer_bps")
    public abstract String customer_bps();

    @SerializedName("consumer_bps")
    public abstract String consumer_bps();

    @SerializedName("gross_margin")
    public abstract String gross_margin();

    @SerializedName("country_all")
    public abstract String country_all();

    public static Builder builder() {
        return new AutoValue_CountryEntity.Builder();
    }

    public static TypeAdapter<CountryEntity> typeAdapter(Gson gson) {
        return new AutoValue_CountryEntity.GsonTypeAdapter(gson);
    }

    @AutoValue.Builder
    public interface Builder{

        Builder id(final String id);
        Builder name(final String name);

        Builder flag(final String flag);
        Builder flag_rectangle(final String flag_rectangle);
        Builder currency(final String currency);
        Builder customer_bps(final String costumer_bps);
        Builder consumer_bps(final String consumer_bps);
        Builder gross_margin(final String gross_margin);
        Builder country_all(final String country_all);

        CountryEntity build();
    }
}
