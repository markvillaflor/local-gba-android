package mcig.module.data.typeAdapter

import com.google.gson.TypeAdapterFactory
import com.ryanharter.auto.value.gson.GsonTypeAdapterFactory

/**
 * Created by republisys on 9/27/17.
 */
@GsonTypeAdapterFactory
abstract class EntityTypeAdapterFactory : TypeAdapterFactory {

    companion object {

        fun create(): EntityTypeAdapterFactory {
            return AutoValueGson_EntityTypeAdapterFactory()
        }
    }

}
