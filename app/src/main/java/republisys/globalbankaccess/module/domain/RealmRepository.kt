package mcig.module.domain

import io.reactivex.Flowable
import io.realm.Realm
import io.realm.RealmObject
import mcig.base.data.store.RealmDataStore
import mcig.base.domain.BaseEntity
import polanski.option.Option

/**
 * Created by republisys on 9/25/17.
 */

abstract class RealmRepository<R: RealmObject, T : BaseEntity>(realm: Realm, restService: RestService) : CacheRepository<T>(restService){

    val entity by lazy {
        RealmDataStore<R, T>(realm)
    }

    fun getAllFromRealm(): Flowable<Option<List<R>>>{
        return this.entity.all()
    }

}
