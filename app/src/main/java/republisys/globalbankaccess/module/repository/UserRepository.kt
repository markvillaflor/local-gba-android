package mcig.module.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

import mcig.module.domain.CacheRepository
import mcig.module.domain.RestService
import republisys.globalbankaccess.module.data.entity.local.CountryEntity
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by republisys on 9/25/17.
 */

/**
 * */
@Singleton
open class UserRepository @Inject constructor(restService: RestService) :
        CacheRepository<CountryEntity>(restService) {

    fun fetchData(): Completable {
        return restService.getCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMapObservable {
                    Observable.fromIterable(it.dataList())
                }
                .toList()
                .doOnSuccess {
                    memoryStore.replaceAll(it)
                }
                .toCompletable()
    }
}